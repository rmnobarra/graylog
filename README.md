## Iniciando a stack:
```bash
sudo sysctl -w vm.max_map_count=262144
docker-compose up -d
```

## Setup graylog:

Acesse o graylog na url: http://127.0.0.1:9000

OBS: user / pass "admin:admin"

* Clique em system > inputs > selecione o tipo de input como GELF TCP e clique em Launch new input.

* Selecione o node a ser utilizado. Adicione um título e escolha a porta que ficará exposta, no exemplo, 12201/tcp.

## Testes

## Setup nginx:

* Em "gelf-address", informe host / porta do graylog

## play nginx compose:
```bash
docker-compose -f nginx/nginx-compose.yaml up -d
```

# ref
[git](https://github.com/vincent-zurczak/fluentbit-configuration-for-k8s-and-graylog)